const gulp = require('gulp');
const del = require('del');
const typescript = require('gulp-typescript');
const webserver = require('gulp-webserver');
const tscConfig = require('./tsconfig.json');

const srcDir = 'app/**/*.ts';
const outDir = 'dist';
const staticFiles = [
  'index.html',
  'styles.css',
  'systemjs.config.js',
];
const browserModules = [
  'node_modules/@angular/**/*',
  'node_modules/ng-semantic/**/*',
  'node_modules/rxjs/**/*',
  'node_modules/zone.js/**/*',
  'node_modules/systemjs/**/*',
  'node_modules/reflect-metadata/**/*',
  'node_modules/core-js/**/*',
];

gulp.task('clean', function () {
  return del(outDir + '/**/*');
});

gulp.task('compile', function () {
  return gulp
    .src(srcDir)
    .pipe(typescript(tscConfig.compilerOptions))
    .pipe(gulp.dest(outDir));
});

gulp.task('static', function() {
  return gulp.src(staticFiles)
    .pipe(gulp.dest(outDir));
});

gulp.task('staticModules', function() {
  return gulp.src(browserModules, {base: '.'})
    .pipe(gulp.dest(outDir));;
});

gulp.task('watch', function() {
  gulp.watch(srcDir, ['compile']);
});

gulp.task('devserver', function() {
  gulp.src(outDir)
    .pipe(webserver({
      port: 3000,
      host: '0.0.0.0',
      livereload: {
        enable: true,
        port: 3001
      },
      proxies: [{
        source: '/api', target: 'http://10.0.2.2:7000/api'
      }]
    }));
});

gulp.task('dev', ['compile', 'watch', 'static', 'staticModules', 'devserver']);
gulp.task('build', ['compile']);
gulp.task('default', ['clean', 'build']);