import { Component } from '@angular/core';
import { User } from './model/User';

@Component({
  selector: 'balancesheet-page',
  template: `
  		<h1>Balance Sheet page</h1>
  		{{message}} 
  		`
})
export class BalanceSheetComponent { 
	message: string = "Balance sheet page";
	
}