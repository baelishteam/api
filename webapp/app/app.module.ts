import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { RouterModule, Routes}   from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent }   from './app.component';
import { LoginComponent } from './login.component';
import { BalanceSheetComponent } from './balancesheet.component';
import { PageNotFoundComponent } from './page-not-found.component';
import { HomeComponent } from './home.component';

// const routes: Routes = [
//  { path: '', redirectTo: '/', pathMatch: 'full' },
//   { path: 'balancesheet',  component: BalanceSheetComponent },
//   { path: 'login', component: LoginComponent },
// ];



@NgModule({
  imports:      [
  	BrowserModule,
  	FormsModule,
  	// RouterModule.forRoot(routes)
    AppRoutingModule
  ],
  declarations: [
  	AppComponent,
    HomeComponent,
  	LoginComponent,
    BalanceSheetComponent,
    PageNotFoundComponent
  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
