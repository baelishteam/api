import { Component } from '@angular/core';
import { User } from './model/User';

@Component({
  selector: 'login-page',
  template: `
  		<h1>Login page</h1>
  		{{message}}
  		<br/>
  		<input type="text" [(ngModel)]="user.login" placeholder="login">
  		<br/>  		
  		<input type="text" [(ngModel)]="user.password" placeholder="password">
  		<br/>
  		<button (click)="onClickMe()">Zaloguj</button>
  		<br/> 
  		`
})
export class LoginComponent { 
	message: string;
	user: User = {};//{login:"", password:""};
	


	 onClickMe() {
     	this.message = 'You trying loggin as: ' + this.user.login + ' password: ' + this.user.password;
    	
   	}
}