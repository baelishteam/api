import { Component } from '@angular/core';



@Component({
  selector: 'home-page',
  template: `
  		<h1>This is home page</h1>
  		{{message}}
  		`
})
export class HomeComponent {
	message: string = "Home page";

}
