import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `
  	<h1>Baelish web app</h1>

    <nav>
      <a routerLink="/home" routerLinkActive="active">Home</a>
      <a routerLink="/login" routerLinkActive="active">Login</a>
      <a routerLink="/balancesheet" routerLinkActive="active">Balance sheet</a>
    </nav>
    <router-outlet></router-outlet>
  	`
})
export class AppComponent {

}
