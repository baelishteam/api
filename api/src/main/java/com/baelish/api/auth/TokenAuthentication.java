package com.baelish.api.auth;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

class TokenAuthentication implements Authentication {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String username;
	private final List<GrantedAuthority> roles;

	public TokenAuthentication(String username, List<String> rolesStrings ) {
		this.username = username;
		this.roles = Collections.unmodifiableList(rolesStrings.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
	}

	@Override
	public String getName() {
		return username;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return roles;
	}

	@Override
	public Object getCredentials() {
		throw new IllegalStateException();
	}

	@Override
	public Object getDetails() {
		return null;
	}

	@Override
	public Object getPrincipal() {
		return username;
	}

	@Override
	public boolean isAuthenticated() {
		return true;
	}

	@Override
	public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
		//TODO: see spec
		throw new IllegalArgumentException();
	}

}
