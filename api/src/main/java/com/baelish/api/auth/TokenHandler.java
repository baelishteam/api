package com.baelish.api.auth;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
class TokenHandler {
	private static final String ROLES_CLAIM = "roles";
	private final byte[] secretKey;
	private final String issuer;
	private final String audience;

	public TokenHandler(
			@Value("${baelish.auth.secret}") String secret,
			@Value("${baelish.auth.issuer}") String issuer,
			@Value("${baelish.auth.audience}") String audience) {
		this.secretKey = secret.getBytes(Charset.forName("UTF-8"));
		this.issuer = issuer;
		this.audience = audience;
	}

	public TokenAuthentication parseUserFromToken(String token) {
		System.out.println("Token handler");
		@SuppressWarnings("unchecked")
		Jwt<Header<?>, Claims> jwt = Jwts.parser()
				.setSigningKey(secretKey)
				.requireAudience(audience)
				.requireIssuer(issuer)
				.parse(token);
		Claims claims = jwt.getBody();
		return new TokenAuthentication(claims.getSubject(), getRoles(claims.get(ROLES_CLAIM, String.class)));
	}
	
	private List<String> getRoles(String roleString) {
		if (roleString == null || roleString.trim().isEmpty()) {
			return Collections.emptyList();
		} else {
			return Arrays.asList(roleString.split(","));
		}
	}

	public String generateToken(User user) {
		Calendar now = new GregorianCalendar();
		Calendar exp = new GregorianCalendar();
		exp.add(Calendar.HOUR, 1);
		return Jwts.builder()
				.setAudience(audience)
				.setIssuer(issuer)
				.setSubject(user.getUsername())
				.setIssuedAt(now.getTime())
				.setExpiration(exp.getTime())
				.setNotBefore(now.getTime())
				.claim(ROLES_CLAIM, getRolesString(user))
				.signWith(SignatureAlgorithm.HS512, secretKey).compact();
	}

	private String getRolesString(User user) {
		return String.join(",", user.getAuthorities().stream().map(a -> a.toString()).collect(Collectors.toList()));
	}
}
