package com.baelish.api.auth;

import java.util.Collections;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

@Component
public class TokenAuthenticationService {
	private static final String AUTH_HEADER_NAME = "Authorization";
	private static final String TOKEN_HEADER_PREFIX = "Bearer ";
	private final TokenHandler tokenHandler;

	@Autowired
	public TokenAuthenticationService(TokenHandler tokenHandler) {
		this.tokenHandler = tokenHandler;
	}

	public String authenticate(String username, String password) {
		if (validCredentials(username, password)) {
			return tokenHandler.generateToken(new User(username, "", Collections.emptyList()));
		} else {
			return null;
		}
	}
	private boolean validCredentials(String username, String password) {
		//TODO: check if user is in db and has correct password (use hashing)
		return "admin".equals(password);
	}
	public Authentication getAuthentication(HttpServletRequest request) {
        final String tokenHeader = request.getHeader(AUTH_HEADER_NAME);
        if (tokenHeader != null) {
        	if (isValidTokenHeader(tokenHeader)) {
        		return tokenHandler.parseUserFromToken(tokenHeader.substring(TOKEN_HEADER_PREFIX.length()));
        	}
            //TODO: throw correct exception
        }
        return null;
    }
	
	private boolean isValidTokenHeader(String tokenHeader) {
		return tokenHeader.startsWith(TOKEN_HEADER_PREFIX);
	}
}
