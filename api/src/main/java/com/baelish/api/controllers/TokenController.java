package com.baelish.api.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.baelish.api.auth.TokenAuthenticationService;
import com.baelish.api.controllers.models.Credentials;
import com.baelish.api.controllers.models.Token;

@RestController
public class TokenController {
	private TokenAuthenticationService authenticationService;

	@Autowired
	public TokenController(TokenAuthenticationService authenticationService) {
		this.authenticationService = authenticationService;
	}
	
	@RequestMapping(path = "/api/tokens", method = RequestMethod.POST)
    public HttpEntity<?> greeting(
            @RequestBody Credentials body) {

		String token = authenticationService.authenticate(body.getUsername(), body.getPassword());
		if (token == null) {
			return ResponseEntity.badRequest().build();
		} else {
			return new ResponseEntity<Token>(new Token(token), HttpStatus.OK);
		}
//        Greeting greeting = new Greeting(String.format(TEMPLATE, name));
//        greeting.add(linkTo(methodOn(GreetingController.class).greeting(name)).withSelfRel());

        
    }
}
