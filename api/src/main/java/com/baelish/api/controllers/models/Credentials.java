package com.baelish.api.controllers.models;

import lombok.Data;

@Data
public class Credentials {
	private String username;
	private String password;
}
