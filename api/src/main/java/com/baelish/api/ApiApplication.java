package com.baelish.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.baelish.api.auth.StatelessAuthenticationFilter;
import com.baelish.api.auth.TokenAuthenticationService;

@SpringBootApplication
public class ApiApplication extends WebSecurityConfigurerAdapter {
	//private TokenHandler tokenHandler;
	private final TokenAuthenticationService tokenAuthenticationService;
	
	public ApiApplication(@Autowired TokenAuthenticationService tokenAuthenticationService) {
		//TokenHandler tokenHandler = new TokenHandler();
		//this.tokenAuthenticationService = new TokenAuthenticationService(tokenHandler);
		this.tokenAuthenticationService = tokenAuthenticationService;
	}
	
	@Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
                .antMatchers("/api/tokens").permitAll()
                .anyRequest().authenticated()
                .and()
                .csrf().disable()
                .addFilterBefore(new StatelessAuthenticationFilter(tokenAuthenticationService), UsernamePasswordAuthenticationFilter.class);;
            /*.formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
            .logout()
                .permitAll();*/
    }

	public static void main(String[] args) {
		SpringApplication.run(ApiApplication.class, args);
	}
}
