package com.baelish.api.auth;

import static org.junit.Assert.*;

import java.util.Collections;

import org.junit.Test;
import org.springframework.security.core.userdetails.User;

public class TokenHandlerTest {
	@Test
	public void shouldGenerateToken() {
		// Given
		TokenHandler handler = new TokenHandler("secret", "issuer", "audience");
		// When
		String token = handler.generateToken(getUser("john"));
		// Then
		assertNotNull(token);
	}

	//TODO: test should parse predefined token

	@Test
	public void shouldGenerateAndParseToken() {
		// Given
		final String username = "john";
		TokenHandler handler = new TokenHandler("secret", "issuer", "audience");
		String token = handler.generateToken(getUser(username));
		System.out.println("TOK: " + token);

		// When
		TokenAuthentication authentication = handler.parseUserFromToken(token);
		// Then
		assertNotNull(authentication);
		assertEquals(username, authentication.getName());
		assertEquals(username, authentication.getPrincipal());
		assertNotNull(authentication.getAuthorities());
		assertEquals(0, authentication.getAuthorities().size());
	}

	private User getUser(String username) {
		return new User(username, "pass", Collections.emptyList());
	}
}
